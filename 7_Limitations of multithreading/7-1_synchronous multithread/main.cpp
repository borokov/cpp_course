#include <iostream>
#include <thread>
#include <chrono>
#include <opencv2/opencv.hpp>
#include <opencv2/opencv.hpp>

void processFrame(cv::Mat frame)
{
    #pragma omp parallel for
    for(int i = 0; i < frame.rows; i++)
    {
        for(int j = 0; j < frame.cols; j++)
        {
            cv::Vec3b& pixel = frame.at<cv::Vec3b>(i, j);
            pixel[0] = 255 - pixel[0];
            pixel[1] = 255 - pixel[1];
            pixel[2] = 255 - pixel[2];
        }
    }
}

int main() {

    // Open the default camera (device index 0). Use DirectShow backend so we can set resolution
    cv::VideoCapture cap(0);

    // Check if the camera opened successfully
    if (!cap.isOpened()) {
        std::cerr << "Error: Could not open the webcam." << std::endl;
        return -1;
    }

    while (true) {
        cv::Mat frame;

        // Read a frame
        cap.read(frame);

        processFrame(frame);

        // Display the frame
        cv::imshow("Webcam", frame);

        // needed, else viewer just shopw blank
        cv::waitKey(25);
    }

    return 0;
}
