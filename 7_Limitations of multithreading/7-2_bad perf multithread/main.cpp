#include <iostream>
#include <thread>
#include <chrono>
#include <TileServer.h>
#include <opencv2/opencv.hpp>

cv::Mat downloadTile(int tileId)
{
    std::vector<uint8_t> rawData = TileServer::httpGet(tileId);

    int tileSize = (int)std::sqrt(rawData.size());
    auto tile = cv::Mat(tileSize, tileSize, CV_8UC1, rawData.data());

    // tile don't have ownsership of data when built like above. We have to clone it to ensure it will keep data alive.
    return tile.clone();
}

void processFrame(cv::Mat frame)
{
    frame.convertTo(frame, frame.type(), -1, 255);
}

int main() 
{
    //#pragma omp parallel for
    for(int tileId = 0; tileId < 1000; tileId++) 
    {
        std::cout << "Tile: " << tileId << std::endl;
        cv::Mat tile = downloadTile(tileId);

        processFrame(tile);
    }

    return 0;
}
