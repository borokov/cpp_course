#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <chrono>


class TileServer
{
public:
    static std::vector<uint8_t> httpGet(int tileId)
    {
        std::lock_guard<std::mutex> lock(s_mutex);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        int tileSize = 256;
        auto tile = std::vector<uint8_t>(tileSize*tileSize);
        return tile;
    }

private:
    static std::mutex s_mutex;
};

std::mutex TileServer::s_mutex;