#include <iostream>
#include <thread>
#include <chrono>
#include <opencv2/opencv.hpp>
#include <opencv2/opencv.hpp>
#include <SharedQueue.hpp>

void readImageRoutine(std::shared_ptr<std::queue<cv::Mat>> queue) {

    // Open the default camera (device index 0). Use DirectShow backend so we can set resolution
    cv::VideoCapture cap(0, cv::CAP_DSHOW);
    // Give big resolution so it set it to max cam resolution
    cap.set(cv::CAP_PROP_FRAME_HEIGHT, 2000);
    cap.set(cv::CAP_PROP_FRAME_WIDTH, 2000);

    // Check if the camera opened successfully
    if (!cap.isOpened()) {
        std::cerr << "Error: Could not open the webcam." << std::endl;
        return;
    }

    while (true) {
        cv::Mat frame;

        // Read a frame
        cap.read(frame);

        cv::Mat upscaledFrame;

        cv::resize(frame, upscaledFrame, cv::Size(4096, 2160));

        queue->push(upscaledFrame);
    }
}

void processImageRoutine(std::shared_ptr<std::queue<cv::Mat>> queueIn, std::shared_ptr<std::queue<cv::Mat>> queueOut)
{
    while (true) {
        if(queueIn->empty())
            continue;

        cv::Mat frame = queueIn->front();
        queueIn->pop();

        cv::Mat gray;
        cv::cvtColor(frame, gray, cv::COLOR_BGR2GRAY);

        // Remove noise
        cv::GaussianBlur(gray, gray, cv::Size(3, 3), 0);

        // Convolute with proper kernels
        cv::Mat sobelx, sobely;
        cv::Sobel(gray, sobelx, CV_32F, 1, 0, 3);  // x
        cv::Sobel(gray, sobely, CV_32F, 0, 1, 3);  // y

        cv::Mat edges;
        cv::add(sobelx, sobely, edges);

        cv::cvtColor(edges, frame, cv::COLOR_GRAY2BGR);

        cv::Mat dowscaledImage;
        cv::resize(frame, dowscaledImage, cv::Size(640, 480));

        queueOut->push(dowscaledImage);
    }
}

void showImageRoutine(std::shared_ptr<std::queue<cv::Mat>> queue)
{
    while (true) {
        if(queue->empty())
            continue;

        cv::Mat frame = queue->front();
        queue->pop();

        // Display the frame (you can replace this with your own processing)
        cv::imshow("Webcam", frame);

        // needed, else viewer just shopw blank
        cv::waitKey(25);
    }
}

void monitorQueueRoutine(std::shared_ptr<std::queue<cv::Mat>> imgQueue, std::shared_ptr<std::queue<cv::Mat>> processedQueue)
{
    while(true)
    {
        std::cout << "imgQueue size: " << imgQueue->size() << "   processedQueue size: " << processedQueue->size() << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

int main() {

    auto imgQueue = std::make_shared<std::queue<cv::Mat>>();
    auto processedQueue = std::make_shared<std::queue<cv::Mat>>();

    std::thread monitorQueueThread(monitorQueueRoutine, imgQueue, processedQueue);

    std::thread producerThread(readImageRoutine, imgQueue);

    std::thread consummerThread1(processImageRoutine, imgQueue, processedQueue);
    
    std::thread showImageThread(showImageRoutine, processedQueue);


    while(true) {}

    return 0;
}
