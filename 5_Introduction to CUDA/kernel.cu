
__global__
void vectorAddition_kernel(float* a, float* b, float* result, int size) {
    int index = threadIdx.x;
    if (index < size) {
        result[index] = a[index] + b[index];
    }
}


void vectorAddition(float* deviceArrayA, float* deviceArrayB, float* deviceResult, int size)
{
    vectorAddition_kernel<<<1, size>>>(deviceArrayA, deviceArrayB, deviceResult, size);
}
