#include <iostream>
#include <cuda_runtime.h>
#include <kernel.cuh>
#include <vector>

int main() {

    const int size = 10;

    // Host arrays
    std::vector<float> hostArrayA(size);
    std::vector<float> hostArrayB(size);
    std::vector<float> hostResult(size);

    // Initialize host arrays
    for (int i = 0; i < size; ++i) {
        hostArrayA[i] = (float)(i);
        hostArrayB[i] = (float)(2 * i);
    }

    // Device arrays
    float* deviceArrayA = nullptr;
    float* deviceArrayB = nullptr;
    float* deviceResult = nullptr;

    cudaError_t cudaError = cudaError_t::cudaSuccess;

    // Allocate memory on the GPU
    cudaError = cudaMalloc((void**)&deviceArrayA, size * sizeof(float));
    cudaError = cudaMalloc((void**)&deviceArrayB, size * sizeof(float));
    cudaError = cudaMalloc((void**)&deviceResult, size * sizeof(float));

    // Copy data from host to device
    cudaError = cudaMemcpy(deviceArrayA, hostArrayA.data(), size * sizeof(float), cudaMemcpyHostToDevice);
    cudaError = cudaMemcpy(deviceArrayB, hostArrayB.data(), size * sizeof(float), cudaMemcpyHostToDevice);

    // Launch the CUDA kernel
    vectorAddition(deviceArrayA, deviceArrayB, deviceResult, size);

    // Copy result from device to host
    cudaError = cudaMemcpy(hostResult.data(), deviceResult, size * sizeof(float), cudaMemcpyDeviceToHost);

    // Display the result
    std::cout << "Result: ";
    for (int i = 0; i < size; ++i) {
        std::cout << hostResult[i] << " ";
    }
    std::cout << std::endl;

    // Free allocated memory on the GPU
    cudaError = cudaFree(deviceArrayA);
    cudaError = cudaFree(deviceArrayB);
    cudaError = cudaFree(deviceResult);

    return 0;
}
