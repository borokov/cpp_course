#include <iostream>
#include <vector>
#include <limits>
#include <omp.h>
#include <random>
#include <chrono>

int main() {
    std::vector<int> numbers(10000000);

    std::cout << "Init with random values" << std::endl;
    for (int i = 0; i < numbers.size(); ++i)
        numbers[i] = std::rand() - 200;


    std::cout << "Find min/max" << std::endl;

    int minVal = std::numeric_limits<int>::max();

    auto t0 = std::chrono::high_resolution_clock::now();

#pragma omp parallel for
    for (int i = 0; i < numbers.size(); ++i) {
        int num = numbers[i];

        //if(num < minVal)
        {
#pragma omp critical
            {
                if (num < minVal) {
                    minVal = num;
                }
            }
        }
    }

    auto t1 = std::chrono::high_resolution_clock::now();

    std::chrono::milliseconds d = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);

    std::cout << "Minimum: " << minVal << " found in " << d.count() << "ms" << std::endl;
    
    return 0;
}