- Présentation du Double-checked locking optimisation
- Éviter les boucle d'attente active
- Nombre idéal de thread ?
    - Tester. Parfois (souvent) les IO sont bottleneck.
	- A la grosse louche: Nb thread <= 2 * Nb core
	- Pour du gros calcul: Nb thread = Nb core - 1 pour garder un coeur de libre pour le system.
