#include <iostream>
#include <vector>
#include <thread>
#include <mutex>

// Function to perform some computation on a subrange of the data
void compute(std::vector<int>& data, std::mutex& mtx, int start, int end) {
    for (int i = start; i < end; ++i) {
        {
            // Lock the mutex to protect the critical section
            std::lock_guard<std::mutex> lock(mtx);

            // Perform computation on data[i]
            data[i] += i;
        } // lock_guard goes out of scope and releases the lock
    }
}

int main() {
    const int size = 1000;
    std::vector<int> data(size, 1);
    std::mutex dataMutex;

    const int num_threads = std::thread::hardware_concurrency();
    const int chunk_size = size / num_threads;

    std::vector<std::thread> threads;

    for (int i = 0; i < num_threads; ++i) {
        int start = i * chunk_size;
        int end = (i == num_threads - 1) ? size : (i + 1) * chunk_size;

        // Launch a thread for each subrange of the data
        threads.emplace_back(compute, std::ref(data), std::ref(dataMutex), start, end);
    }

    // Join all threads to wait for them to complete
    for (auto& thread : threads) {
        thread.join();
    }

    // Print the results
    for (int i = 0; i < size; ++i) {
        std::cout << data[i] << " ";
    }

    std::cout << std::endl;

    return 0;
}
