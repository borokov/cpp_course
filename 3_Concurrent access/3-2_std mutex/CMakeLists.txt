find_package(Threads REQUIRED)

add_executable(3-2_std_mutex_example main.cpp)

target_link_libraries(3-2_std_mutex_example PRIVATE Threads::Threads)
