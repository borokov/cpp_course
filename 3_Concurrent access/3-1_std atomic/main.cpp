#include <iostream>
#include <vector>
#include <thread>
#include <atomic>

// Function to perform some computation on a subrange of the data
void compute(std::vector<std::atomic<int>>& data, int start, int end) {
    for (int i = start; i < end; ++i) {
        // Perform computation on data[i] using atomic operations
        data[i].fetch_add(i, std::memory_order_relaxed);
    }
}

int main() {
    const int size = 1000;
    std::vector<std::atomic<int>> data(size);

    const int num_threads = std::thread::hardware_concurrency();
    const int chunk_size = size / num_threads;

    std::vector<std::thread> threads;

    for (int i = 0; i < size; ++i) {
        data[i] = 1;
    }

    for (int i = 0; i < num_threads; ++i) {
        int start = i * chunk_size;
        int end = (i == num_threads - 1) ? size : (i + 1) * chunk_size;

        // Launch a thread for each subrange of the data
        threads.emplace_back(compute, std::ref(data), start, end);
    }

    // Join all threads to wait for them to complete
    for (auto& thread : threads) {
        thread.join();
    }

    // Print the results
    for (int i = 0; i < size; ++i) {
        std::cout << data[i] << " ";
    }

    std::cout << std::endl;

    return 0;
}
