project(4_Profiling_with_Optick)

add_executable(4_Profiling_with_Optick 
main.cpp 
PhysicEngine.cpp
RenderEngine.cpp
)

set(Optick_LIBS "D:/Objectif3D/Optick/lib/x64/release/OptickCore.lib")
target_link_libraries(4_Profiling_with_Optick ${Optick_LIBS})

set(Optick_INCLUDE_DIR "D:/Objectif3D/Optick/include")
target_include_directories(4_Profiling_with_Optick PUBLIC ${Optick_INCLUDE_DIR} ${CMAKE_CURRENT_SOURCE_DIR})

target_compile_definitions(4_Profiling_with_Optick PUBLIC USE_OPTICK=1)
