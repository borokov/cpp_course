#include <PhysicEngine.h>
#include <RenderEngine.h>
#include <optick.h>
#include <iostream>

int main() {

    while(true)
    {
        OPTICK_FRAME("MainLoop");
        std::cout << "frame begin" << std::endl;

        PhysicEngine::apply();

        RenderEngine::render();

        std::cout << "frame end" << std::endl;
    }

    return 0;
}
