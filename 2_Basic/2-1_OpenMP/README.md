OpenMP makes it easy to parallelize loops in C++. Here's a minimal example of a CMakeLists.txt file and a C++ source code file that uses OpenMP for a parallel loop:

This example assumes that you have CMake installed and a C++ compiler that supports OpenMP (such as GCC or Clang). The CMakeLists.txt file sets up the project and links against the OpenMP library. The main.cpp file contains a simple parallelized loop using the #pragma omp parallel for directive.

You can customize the size of the loop and the loop body according to your needs. This example demonstrates a basic usage of OpenMP for parallelizing a loop in C++.