find_package(OpenMP REQUIRED)

add_executable(2-1_OpenMP_example main.cpp)

target_compile_options(2-1_OpenMP_example PRIVATE ${OpenMP_CXX_FLAGS})
target_link_libraries(2-1_OpenMP_example PRIVATE OpenMP::OpenMP_CXX)
