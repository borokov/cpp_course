#include <iostream>
#include <vector>

int main() {
    const int size = 1000;
    std::vector<int> data(size, 1);

    #pragma omp parallel for
    for (int i = 0; i < size; ++i) {
        // Parallelized loop body
        data[i] += i;
    }

    // Print the results
    for (int i = 0; i < size; ++i) {
        std::cout << data[i] << " ";
    }

    std::cout << std::endl;

    return 0;
}
