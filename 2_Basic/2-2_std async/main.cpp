#include <iostream>
#include <vector>
#include <future>

// Function to perform some computation on a subrange of the data
void compute(std::vector<int>& data, int start, int end) {
    for (int i = start; i < end; ++i) {
        // Perform computation on data[i]
        data[i] += i;
    }
}

int main() {
    const int size = 1000;
    std::vector<int> data(size, 1);

    const int num_threads = std::thread::hardware_concurrency();
    const int chunk_size = size / num_threads;

    std::vector<std::future<void>> futures;

    for (int i = 0; i < num_threads; ++i) {
        int start = i * chunk_size;
        int end = (i == num_threads - 1) ? size : (i + 1) * chunk_size;

        // Launch a thread asynchronously for each subrange of the data
        futures.push_back(std::async(std::launch::async, compute, std::ref(data), start, end));
    }

    // Wait for all asynchronous tasks to complete
    for (auto& future : futures) {
        future.wait();
    }

    // Print the results
    for (int i = 0; i < size; ++i) {
        std::cout << data[i] << " ";
    }

    std::cout << std::endl;

    return 0;
}
