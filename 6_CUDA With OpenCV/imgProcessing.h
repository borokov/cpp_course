#include <opencv2/opencv.hpp>


void convertToGreyScale(cv::InputArray input, cv::OutputArray ouput);
void convertToGreyCUDA(cv::InputArray input, cv::OutputArray ouput);


void convertToColor(cv::InputArray input, cv::OutputArray ouput);

std::pair<float, float> computeMinMax(cv::Mat grayscale);

void normalize(cv::Mat grayscale, float min, float max);