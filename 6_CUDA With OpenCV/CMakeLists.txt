project(6_CUDA_With_OpenCV LANGUAGES CXX CUDA)

# set path to fodler containing "OpenCVConfig.cmake"
list(APPEND CMAKE_PREFIX_PATH "D:/Objectif3D/opencv/build")

find_package(OpenCV REQUIRED)

add_executable(6_CUDA_With_OpenCV 
main.cpp
imgProcessing.cpp
kernel.cu
)

set_property(TARGET 6_CUDA_With_OpenCV PROPERTY CUDA_ARCHITECTURES 50 60 75)

target_include_directories(6_CUDA_With_OpenCV PUBLIC ${CMAKE_CURRENT_SOURCE_DIR} ${OpenCV_INCLUDE_DIRS})

target_link_libraries(6_CUDA_With_OpenCV PRIVATE ${OpenCV_LIBS})
