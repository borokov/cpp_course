#include <cuda_runtime.h>
#include <imgProcessing.h>
#include <kernel.cuh>


void convertToGreyScale(cv::InputArray input, cv::OutputArray ouput) {
  cv::cvtColor(input, ouput, cv::COLOR_BGR2GRAY);
}

void convertToColor(cv::InputArray input, cv::OutputArray ouput) {
  cv::cvtColor(input, ouput, cv::COLOR_GRAY2BGR);
}

void convertToGreyCUDA(cv::InputArray input, cv::OutputArray ouput) {
  // Image dimensions
  int width = input.cols();
  int height = input.rows();
  int imageSize = width * height * 3; // 3 bytes per pixel for RGB

  // Allocate host memory for input and output images
  unsigned char* h_inputImage = input.getMat().data;

  cudaError_t cudaError = cudaError_t::cudaSuccess;

  // Allocate device memory for input and output images
  unsigned char* d_inputImage;
  unsigned char* d_outputImage;
  cudaError = cudaMalloc((void **)&d_inputImage, imageSize * sizeof(unsigned char));
  cudaError = cudaMalloc((void **)&d_outputImage, width * height * sizeof(unsigned char));

  // Copy input image data from host to device
  cudaError = cudaMemcpy(d_inputImage, h_inputImage, imageSize * sizeof(unsigned char), cudaMemcpyHostToDevice);

  convertToGreyCUDA(d_inputImage, d_outputImage, width, height);

  // Copy the resulting grayscale image data from device to host
  unsigned char* h_outputImage = new unsigned char[width * height];
  cudaError = cudaMemcpy(h_outputImage, d_outputImage, width * height * sizeof(unsigned char), cudaMemcpyDeviceToHost);

  // Free device memory
  cudaFree(d_inputImage);
  cudaFree(d_outputImage);

  cv::Mat image(height, width, CV_8UC1, h_outputImage);

  ouput.assign(image);
}

std::pair<float, float> computeMinMax(cv::Mat grayscale) {
  float min = 0;
  float max = 0;
  for (int i = 0; i < grayscale.rows; ++i) {
    for (int j = 0; j < grayscale.cols; ++j) {
      float pixelValue = static_cast<float>(grayscale.at<uchar>(i, j)) / 255.0f;
      min = std::min(min, pixelValue);
      max = std::max(max, pixelValue);
    }
  }
  return std::make_pair(min, max);
}

void normalize(cv::Mat grayscale, float min, float max) {
  for (int i = 0; i < grayscale.rows; ++i) {
    for (int j = 0; j < grayscale.cols; ++j) {
      uchar pixelValue = grayscale.at<uchar>(i, j);
      grayscale.at<uchar>(i, j) =
          (uchar)(((float)pixelValue - min) / (max - min));
    }
  }
}
