#include <iostream>
#include <opencv2/opencv.hpp>
#include <imgProcessing.h>

int main() 
{
    cv::VideoCapture cap("D:/Objectif3D/cpp_course/6_CUDA With OpenCV/Media1.mp4");

    while (true) 
    {
        cv::Mat frame;

        // Read a frame
        cap.read(frame);

        int newWidth = 512;
        int newHeight = 512;
        // Resize the image
        cv::Mat resizedImage;
        cv::resize(frame, resizedImage, cv::Size(newWidth, newHeight));

        cv::Mat greyScaleImage;


        convertToGreyCUDA(resizedImage, greyScaleImage);

        cv::Mat img;

        convertToColor(greyScaleImage, img);


        // Display the frame
        cv::imshow("Webcam", img);

        // needed, else viewer just shopw blank
        cv::waitKey(25);
    }



    //std::pair<float, float> min_max = computeMinMax(greyScaleImage);

    //normalize(greyScaleImage, min_max.first, min_max.second);

    return 0;
}
