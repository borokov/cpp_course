__global__ 
void convertToGrey_kernel(const uint8_t* inputImage, uint8_t* outputImage, int width, int height) 
{
    const int i = blockDim.x * blockIdx.x + threadIdx.x;
    const int j = blockDim.y * blockIdx.y + threadIdx.y;
    if (j >= height || i >= width)
        return;

    int tid = j * width + i;
    unsigned char r = inputImage[tid * 3];
    unsigned char g = inputImage[tid * 3 + 1];
    unsigned char b = inputImage[tid * 3 + 2];
    outputImage[tid] = 0.299f * r + 0.587f * g + 0.114f * b;
}

void convertToGreyCUDA(const uint8_t* colorImg, uint8_t* greyImg, int width, int height)
{
    //dim3 blockSize(16, 16);
    //dim3 gridSize((width + blockSize.x - 1) / blockSize.x, (height + blockSize.y - 1) / blockSize.y);

    dim3 threadsPerBlock(16, 16);
    dim3 numBlocks((int)std::ceil((float)width / (float)threadsPerBlock.x), (int)std::ceil((float)height / (float)threadsPerBlock.y));


    convertToGrey_kernel<<<threadsPerBlock , numBlocks>>>(colorImg, greyImg, width, height);
}
